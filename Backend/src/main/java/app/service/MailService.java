package app.service;

import app.model.dto.MailContextDto;
import app.exception.MailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class MailService {

    private static final String USERNAME = "cloroplast2@gmail.com";
    private static final String PASSWORD = "parolalamail123";
    private final Properties props;

    public MailService() {
        props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
    }

    public void sendMail(MailContextDto mailContextDto) {
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(USERNAME, PASSWORD);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(USERNAME));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(mailContextDto.getTo()));
            message.setSubject(mailContextDto.getSubject());
            message.setText(mailContextDto.getContent());

            Transport.send(message);

        } catch (MessagingException e) {
            throw new MailException(e);
        }
    }


}

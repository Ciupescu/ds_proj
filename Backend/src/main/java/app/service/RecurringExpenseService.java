package app.service;

import app.exception.BudgetNotFoundException;
import app.exception.ExpenseNotFoundException;
import app.model.dto.MailContextDto;
import app.model.dto.RecurringExpenseDto;
import app.model.entity.Budget;
import app.model.entity.ExpenseLog;
import app.model.entity.RecurringExpense;
import app.repository.BudgetRepository;
import app.repository.ExpenseLogRepository;
import app.repository.RecurringExpenseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.*;

@Service
public class RecurringExpenseService {

    public static final String RECURRING_EXPENSES = "Recurring Expenses";
    private RecurringExpenseRepository recurringExpenseRepository;
    private BudgetRepository budgetRepository;
    private ExpenseLogRepository expenseLogRepository;
    private MailService mailService;
    private boolean mailNotificationsEnabled;

    public RecurringExpenseService() {
    }

    @Autowired
    public RecurringExpenseService(RecurringExpenseRepository recurringExpenseRepository, BudgetRepository budgetRepository, ExpenseLogRepository expenseLogRepository, MailService mailService) {
        this.recurringExpenseRepository = recurringExpenseRepository;
        this.budgetRepository = budgetRepository;
        this.expenseLogRepository = expenseLogRepository;
        this.mailService = mailService;
        this.mailNotificationsEnabled = true;
    }

    public List<RecurringExpenseDto> getRecurringExpensesBudget(long budgetId) {
        return recurringExpenseRepository.findAllByBudgetId(budgetId)
                .stream()
                .map(RecurringExpenseDto::new)
                .collect(Collectors.toList());
    }

    public List<RecurringExpenseDto> getRecurringExpensesValid(long budgetId) {
        return recurringExpenseRepository.findAllByBudgetId(budgetId)
                .stream()
                .filter(e -> LocalDate.now().isBefore(e.getExpirationDate()))
                .map(RecurringExpenseDto::new)
                .collect(Collectors.toList());
    }

    public long addRecurringExpense(RecurringExpenseDto recurringExpenseDto) {
        RecurringExpense recurringExpense = new RecurringExpense();
        recurringExpense.setDescription(recurringExpenseDto.getDescription());
        Optional<Budget> budgetOptional = budgetRepository.findById(recurringExpenseDto.getBudgetId());
        if (budgetOptional.isEmpty()) {
            throw new BudgetNotFoundException();
        }
        Budget budget = budgetOptional.get();
        recurringExpense.setBudget(budget);
        recurringExpense.setAmount(recurringExpenseDto.getAmount());
        recurringExpense.setExpirationDate(recurringExpenseDto.getExpirationDate());
        recurringExpense.setLastOccurence(recurringExpenseDto.getLastOccurence());
        recurringExpense.setRecurrence(recurringExpenseDto.getRecurrence());
        recurringExpenseRepository.saveAndFlush(recurringExpense);
        return recurringExpense.getId();
    }

    public void updateRecurringExpense(RecurringExpenseDto recurringExpenseDto) {
        Optional<RecurringExpense> recurringExpenseOptional = recurringExpenseRepository.findById(recurringExpenseDto.getId());
        if (recurringExpenseOptional.isEmpty()) {
            throw new ExpenseNotFoundException();
        }

        Optional<Budget> budgetOptional = budgetRepository.findById(recurringExpenseDto.getBudgetId());
        if (budgetOptional.isEmpty()) {
            throw new BudgetNotFoundException();
        }

        RecurringExpense recurringExpense = recurringExpenseOptional.get();
        Budget budget = budgetOptional.get();
        recurringExpense.setBudget(budget);
        recurringExpense.setAmount(recurringExpenseDto.getAmount());
        recurringExpense.setDescription(recurringExpenseDto.getDescription());
        recurringExpense.setExpirationDate(recurringExpenseDto.getExpirationDate());
        recurringExpense.setLastOccurence(recurringExpenseDto.getLastOccurence());
        recurringExpense.setRecurrence(recurringExpenseDto.getRecurrence());

        recurringExpenseRepository.saveAndFlush(recurringExpense);
    }

    public void removeRecurringExpense(long recurringExpenseId) {
        Optional<RecurringExpense> recurringExpenseOptional = recurringExpenseRepository.findById(recurringExpenseId);
        if (recurringExpenseOptional.isEmpty()) {
            throw new ExpenseNotFoundException();
        }

        RecurringExpense recurringExpense = recurringExpenseOptional.get();
        recurringExpenseRepository.delete(recurringExpense);
        recurringExpenseRepository.flush();
    }

    public void enableMailNotifications(boolean enabled) {
        mailNotificationsEnabled = enabled;
    }

    @PostConstruct
    private void checkRecurringExpensesOnStartup() {
        checkRecurringExpenses();
    }

    @Scheduled(cron = "0 0 0 * * ?")
    private void checkRecurringExpensesScheduled() {
        checkRecurringExpenses();
    }

    private void checkRecurringExpenses() {
        var expenses = recurringExpenseRepository.findAll()
                .stream()
                .filter(e -> LocalDate.now().isBefore(e.getExpirationDate()))
                .filter(this::expenseDue)
                .collect(Collectors.toList());
        if (mailNotificationsEnabled) {
            sendMailNotification(expenses);
        }
        expenses.forEach(this::logExpense);
    }

    private void sendMailNotification(List<RecurringExpense> expenses) {
        if (expenses.isEmpty()) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("The following recurring expenses have been applied:\n");
        expenses
                .stream()
                .map(RecurringExpenseDto::new)
                .forEach(b -> {
                    sb.append(b);
                    sb.append("\n");
                });
        MailContextDto mailContextDto = new MailContextDto("ciucescuv@outlook.com", RECURRING_EXPENSES, sb.toString());
        mailService.sendMail(mailContextDto);
    }

    private boolean expenseDue(RecurringExpense expense) {
        var lastOccurence = expense.getLastOccurence();
        var now = LocalDate.now();
        switch (expense.getRecurrence()) {
            case DAILY:
                return DAYS.between(lastOccurence, now) >= 1;
            case WEEKLY:
                return WEEKS.between(lastOccurence, now) >= 1;
            case MONTHLY:
                return MONTHS.between(lastOccurence, now) >= 1;
            case YEARLY:
                return YEARS.between(lastOccurence, now) >= 1;
            default:
                return false;
        }
    }

    private void logExpense(RecurringExpense recurringExpense) {
        recurringExpense.setLastOccurence(LocalDate.now());
        Budget budget = recurringExpense.getBudget();
        budget.setAmountConsumed(budget.getAmountConsumed() + recurringExpense.getAmount());
        budgetRepository.saveAndFlush(budget);
        recurringExpenseRepository.saveAndFlush(recurringExpense);
        ExpenseLog expenseLog = new ExpenseLog(recurringExpense);
        expenseLogRepository.saveAndFlush(expenseLog);
    }
}

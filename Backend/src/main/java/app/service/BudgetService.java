package app.service;

import app.model.Currency;
import app.model.dto.MailContextDto;
import app.model.entity.Budget;
import app.model.entity.Category;
import app.model.dto.BudgetDto;
import app.exception.BudgetNotFoundException;
import app.exception.CategoryNotFoundException;
import app.repository.BudgetRepository;
import app.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.*;

@Service
public class BudgetService {

    public static final String BUDGETS_RENEWED = "Budgets Renewed";
    private BudgetRepository budgetRepository;
    private CategoryRepository categoryRepository;
    private MailService mailService;
    private boolean mailNotificationsEnabled;

    public BudgetService() {
    }

    @Autowired
    public BudgetService(BudgetRepository budgetRepository, CategoryRepository categoryRepository, MailService mailService) {
        this.budgetRepository = budgetRepository;
        this.categoryRepository = categoryRepository;
        this.mailService = mailService;
        this.mailNotificationsEnabled = true;
    }

    public BudgetDto getBudgetById(long budgetId) {
        Optional<Budget> budgetOptional = budgetRepository.findById(budgetId);
        if (budgetOptional.isEmpty()) {
            throw new BudgetNotFoundException();
        }
        Budget budget = budgetOptional.get();
        return new BudgetDto(budget);
    }

    public List<BudgetDto> getBudgetsCategory(long categoryId) {
        return budgetRepository.findAllByCategoryId(categoryId)
                .stream()
                .map(BudgetDto::new)
                .collect(Collectors.toList());
    }

    public List<BudgetDto> getBudgets() {
        return budgetRepository.findAll()
                .stream()
                .map(BudgetDto::new)
                .collect(Collectors.toList());
    }

    public long addBudget(BudgetDto budgetDto) {
        Budget budget = new Budget();
        budget.setCurrency(Currency.get(budgetDto.getCurrency()));
        budget.setAmount(budgetDto.getAmount());
        Optional<Category> categoryOptional = categoryRepository.findById(budgetDto.getCategoryId());
        if (categoryOptional.isEmpty()) {
            throw new CategoryNotFoundException();
        }
        Category category = categoryOptional.get();
        budget.setCategory(category);
        budget.setDescription(budgetDto.getDescription());
        budget.setValability(budgetDto.getValability());
        budget.setAmountConsumed(budgetDto.getAmountConsumed());
        budget.setLastRenewal(budgetDto.getLastRenewal());
        budgetRepository.saveAndFlush(budget);
        return budget.getId();
    }

    public void updateBudget(BudgetDto budgetDto) {
        Optional<Budget> budgetOptional = budgetRepository.findById(budgetDto.getId());
        if (budgetOptional.isEmpty()) {
            throw new BudgetNotFoundException();
        }

        Optional<Category> categoryOptional = categoryRepository.findById(budgetDto.getCategoryId());
        if (categoryOptional.isEmpty()) {
            throw new CategoryNotFoundException();
        }

        Budget budget = budgetOptional.get();
        Category category = categoryOptional.get();
        budget.setCategory(category);
        budget.setDescription(budgetDto.getDescription());
        budget.setAmount(budgetDto.getAmount());
        budget.setCurrency(Currency.get(budgetDto.getCurrency()));
        budget.setValability(budgetDto.getValability());
        budget.setAmountConsumed(budgetDto.getAmountConsumed());
        budget.setLastRenewal(budgetDto.getLastRenewal());
        budgetRepository.saveAndFlush(budget);

    }

    public void removeBudget(long id) {
        Optional<Budget> budgetOptional = budgetRepository.findById(id);
        if (budgetOptional.isEmpty()) {
            throw new BudgetNotFoundException();
        }

        Budget budget = budgetOptional.get();
        budgetRepository.delete(budget);
        budgetRepository.flush();
    }

    public void enableMailNotifications(boolean enabled) {
        mailNotificationsEnabled = enabled;
    }

    @PostConstruct
    private void renewBudgetsOnStartup() {
        renewBudgets();
    }

    @Scheduled(cron = "0 0 0 * * ?")
    private void renewBudgetScheduled() {
        renewBudgets();
    }

    private void renewBudgets() {
        var renewableBudgets = budgetRepository.findAll()
                .stream()
                .filter(this::renewableBudget)
                .collect(Collectors.toList());
        if (mailNotificationsEnabled) {
            sendMailNotification(renewableBudgets);
        }
        renewableBudgets.forEach(this::renewBudget);
    }

    private void sendMailNotification(List<Budget> renewableBudgets) {
        if (renewableBudgets.isEmpty()) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("The following budgets have been renewed:\n");
        renewableBudgets
                .stream()
                .map(BudgetDto::new)
                .forEach(b -> {
            sb.append(b);
            sb.append("\n");
        });
        MailContextDto mailContextDto = new MailContextDto("ciucescuv@outlook.com", BUDGETS_RENEWED, sb.toString());
        mailService.sendMail(mailContextDto);
    }

    private boolean renewableBudget(Budget budget) {
        var lastRenewal = budget.getLastRenewal();
        var now = LocalDate.now();
        switch (budget.getValability()) {
            case DAILY: return DAYS.between(lastRenewal, now) >=1;
            case WEEKLY: return WEEKS.between(lastRenewal, now) >= 1;
            case MONTHLY: return MONTHS.between(lastRenewal, now) >= 1;
            case YEARLY: return YEARS.between(lastRenewal, now) >= 1;
            default: return false;
        }
    }

    private void renewBudget(Budget budget) {
        budget.setLastRenewal(LocalDate.now());
        budget.setAmountConsumed(0);
        budgetRepository.saveAndFlush(budget);
    }
}

package app.service;

import app.exception.CategoryDeleteException;
import app.model.BaseCategory;
import app.model.entity.Category;
import app.model.dto.CategoryDto;
import app.exception.CategoryNotFoundException;
import app.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CategoryService {

    private CategoryRepository categoryRepository;

    public CategoryService() {
    }

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public CategoryDto getCategoryById(long categoryId) {
        Optional<Category> categoryOptional = categoryRepository.findById(categoryId);
        if (categoryOptional.isEmpty()) {
            throw new CategoryNotFoundException();
        }
        Category category = categoryOptional.get();
        return new CategoryDto(category);
    }

    public List<CategoryDto> getCategories() {
        return categoryRepository.findAll()
                .stream()
                .map(CategoryDto::new)
                .collect(Collectors.toList());
    }

    public List<CategoryDto> getCategoriesForBase(BaseCategory baseCategory) {
        return categoryRepository.findAllByBaseCategory(baseCategory)
                .stream()
                .map(CategoryDto::new)
                .collect(Collectors.toList());
    }

    public long addCategory(CategoryDto categoryDto) {
        Category category = new Category();
        category.setBaseCategory(categoryDto.getBaseCategory());
        category.setDescription(categoryDto.getDescription());
        category.setName(categoryDto.getName());
        categoryRepository.saveAndFlush(category);
        return category.getId();
    }

    public void updateCategory(CategoryDto categoryDto) {
        Optional<Category> categoryOptional = categoryRepository.findById(categoryDto.getId());
        if (categoryOptional.isEmpty()) {
            throw new CategoryNotFoundException();
        }

        Category category = categoryOptional.get();
        category.setName(categoryDto.getName());
        category.setDescription(categoryDto.getDescription());
        category.setBaseCategory(categoryDto.getBaseCategory());
        categoryRepository.saveAndFlush(category);
    }

    public void removeCategory(long categId) {
        Optional<Category> categoryOptional = categoryRepository.findById(categId);
        if (categoryOptional.isEmpty()) {
            throw new CategoryNotFoundException();
        }

        Category category = categoryOptional.get();
        try {
            categoryRepository.delete(category);
            categoryRepository.flush();
        } catch (Exception e) {
            throw new CategoryDeleteException();
        }
    }
}

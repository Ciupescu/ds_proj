package app.service;

import app.criteria.CriteriaUtil;
import app.exception.BudgetNotFoundException;
import app.exception.ExpenseNotFoundException;
import app.model.dto.CriterionDto;
import app.model.dto.ExpenseLogDto;
import app.model.entity.Budget;
import app.model.entity.ExpenseLog;
import app.repository.BudgetRepository;
import app.repository.ExpenseLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ExpenseLogService {

    private ExpenseLogRepository expenseLogRepository;
    private BudgetRepository budgetRepository;

    public ExpenseLogService() {
    }

    @Autowired
    public ExpenseLogService(ExpenseLogRepository expenseLogRepository, BudgetRepository budgetRepository) {
        this.expenseLogRepository = expenseLogRepository;
        this.budgetRepository = budgetRepository;
    }

    public List<ExpenseLogDto> getExpenseLogsBudget(long budgetId) {
        return expenseLogRepository.findAllByBudgetId(budgetId)
                .stream()
                .map(ExpenseLogDto::new)
                .collect(Collectors.toList());
    }

    public long addExpenseLog(ExpenseLogDto expenseLogDto) {
        ExpenseLog expenseLog = new ExpenseLog();
        expenseLog.setDescription(expenseLogDto.getDescription());
        Optional<Budget> budgetOptional = budgetRepository.findById(expenseLogDto.getBudgetId());
        if (budgetOptional.isEmpty()) {
            throw new BudgetNotFoundException();
        }
        Budget budget = budgetOptional.get();
        budget.setAmountConsumed(budget.getAmountConsumed() + expenseLogDto.getAmount());
        budgetRepository.saveAndFlush(budget);
        expenseLog.setBudget(budget);
        expenseLog.setAmount(expenseLogDto.getAmount());
        expenseLog.setTimestamp(LocalDateTime.parse(expenseLogDto.getTimestamp(), ExpenseLogDto.inputFormatter));
        expenseLogRepository.saveAndFlush(expenseLog);
        return expenseLog.getId();
    }

    public void deleteExpenseLog(long expenseLogId) {
        Optional<ExpenseLog> expenseOptional = expenseLogRepository.findById(expenseLogId);
        if (expenseOptional.isEmpty()) {
            throw new ExpenseNotFoundException();
        }

        ExpenseLog expenseLog = expenseOptional.get();
        expenseLogRepository.delete(expenseLog);
        expenseLogRepository.flush();
    }

    public List<ExpenseLogDto> filterExpenses(List<CriterionDto> criteria) {
        Specification<ExpenseLog> specification = CriteriaUtil.getCriteriaSpecification(criteria);
        return expenseLogRepository.findAll(specification)
                .stream()
                .map(ExpenseLogDto::new)
                .collect(Collectors.toList());
    }

}

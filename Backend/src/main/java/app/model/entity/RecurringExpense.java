package app.model.entity;

import app.model.Period;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "recurring_expenses")
public class RecurringExpense extends BaseExpense {

    @Column(name = "last_occurence")
    private LocalDate lastOccurence;

    @Enumerated(EnumType.STRING)
    private Period recurrence;

    @Column(name = "expiration_date")
    private LocalDate expirationDate;

    public LocalDate getLastOccurence() {
        return lastOccurence;
    }

    public void setLastOccurence(LocalDate lastOccurence) {
        this.lastOccurence = lastOccurence;
    }

    public Period getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(Period recurrence) {
        this.recurrence = recurrence;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }
}

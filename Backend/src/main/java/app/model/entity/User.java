package app.model.entity;

import javax.persistence.Column;

public class User extends BaseEntity {

    @Column
    private byte[] email;

    @Column
    private byte[] password;

    public byte[] getEmail() {
        return email;
    }

    public void setEmail(byte[] email) {
        this.email = email;
    }

    public byte[] getPassword() {
        return password;
    }

    public void setPassword(byte[] password) {
        this.password = password;
    }
}

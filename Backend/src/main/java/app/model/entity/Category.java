package app.model.entity;

import app.model.BaseCategory;

import javax.persistence.*;

@Entity
@Table(name = "categories")
public class Category extends BaseEntity {

    @Column(length = 45)
    private String name;

    @Column(length = 100)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "base_category")
    private BaseCategory baseCategory;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BaseCategory getBaseCategory() {
        return baseCategory;
    }

    public void setBaseCategory(BaseCategory baseCategory) {
        this.baseCategory = baseCategory;
    }
}

package app.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "base_expenses")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class BaseExpense extends BaseEntity {

    @ManyToOne(targetEntity = Budget.class)
    private Budget budget;

    @Column(length = 100)
    private String description;

    @Column
    private double amount;

    public Budget getBudget() {
        return budget;
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}

package app.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "expense_logs")
public class ExpenseLog extends BaseExpense {

    @Column
    private LocalDateTime timestamp;

    public ExpenseLog() {
    }

    public ExpenseLog(RecurringExpense recurringExpense) {
        setId(recurringExpense.getId());
        setAmount(recurringExpense.getAmount());
        setBudget(recurringExpense.getBudget());
        setDescription(recurringExpense.getDescription());
        setTimestamp(LocalDateTime.now());
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }
}

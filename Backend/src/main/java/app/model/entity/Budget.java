package app.model.entity;

import app.model.Currency;
import app.model.Period;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Table(name = "budgets")
public class Budget extends BaseEntity {

    @Enumerated(EnumType.STRING)
    private Currency currency;

    @Column
    private double amount;

    @ManyToOne(targetEntity = Category.class)
    private Category category;

    @Column(length = 100)
    private String description;

    @Enumerated(EnumType.STRING)
    private Period valability;

    @Column(name = "amount_consumed")
    private double amountConsumed;

    @Column(name = "last_renewal")
    private LocalDate lastRenewal;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Period getValability() {
        return valability;
    }

    public void setValability(Period valability) {
        this.valability = valability;
    }

    public double getAmountConsumed() {
        return amountConsumed;
    }

    public void setAmountConsumed(double amountConsumed) {
        this.amountConsumed = amountConsumed;
    }

    public LocalDate getLastRenewal() {
        return lastRenewal;
    }

    public void setLastRenewal(LocalDate lastRenewal) {
        this.lastRenewal = lastRenewal;
    }
}

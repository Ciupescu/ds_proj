package app.model;

public enum Period {

    DAILY,
    WEEKLY,
    MONTHLY,
    YEARLY
}

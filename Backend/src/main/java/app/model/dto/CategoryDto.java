package app.model.dto;

import app.model.entity.Category;
import app.model.BaseCategory;

public class CategoryDto extends BaseDto {

    private final String name;
    private final String description;
    private final BaseCategory baseCategory;

    public CategoryDto() {
        name = "";
        description = "";
        baseCategory = BaseCategory.CLOTHES;
    }

    public CategoryDto(long id, String name, String description, BaseCategory baseCategory) {
        super(id);
        this.name = name;
        this.description = description;
        this.baseCategory = baseCategory;
    }

    public CategoryDto(Category category) {
        super(category);
        name = category.getName();
        description = category.getDescription();
        baseCategory = category.getBaseCategory();
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public BaseCategory getBaseCategory() {
        return baseCategory;
    }
}

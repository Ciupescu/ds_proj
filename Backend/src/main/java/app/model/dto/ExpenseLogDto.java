package app.model.dto;

import app.model.entity.ExpenseLog;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class ExpenseLogDto extends BaseExpenseDto {

    public static final DateTimeFormatter outputFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
    public static final DateTimeFormatter inputFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

    private final String timestamp;

    public ExpenseLogDto() {
        timestamp = LocalDateTime.now().format(outputFormatter);
    }

    public ExpenseLogDto(long id, long budgetId, String description, double amount,
                         LocalDateTime timestamp) {
        super(id, budgetId, description, amount);
        this.timestamp = timestamp.format(outputFormatter);
    }

    public ExpenseLogDto(ExpenseLog expenseLog) {
        super(expenseLog);
        this.timestamp = expenseLog.getTimestamp().format(outputFormatter);
    }

    public String getTimestamp() {
        return timestamp;
    }
}

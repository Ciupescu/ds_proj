package app.model.dto;

import app.model.Period;
import app.model.entity.RecurringExpense;

import java.time.LocalDate;

public class RecurringExpenseDto extends BaseExpenseDto {

    private final LocalDate lastOccurence;
    private final Period recurrence;
    private final LocalDate expirationDate;

    public  RecurringExpenseDto() {
        lastOccurence = LocalDate.now();
        recurrence = Period.DAILY;
        expirationDate = LocalDate.now();
    }

    public RecurringExpenseDto(long id, long budgetId, String description, double amount, LocalDate lastOccurence, Period recurrence, LocalDate expirationDate) {
        super(id, budgetId, description, amount);
        this.lastOccurence = lastOccurence;
        this.recurrence = recurrence;
        this.expirationDate = expirationDate;
    }

    public RecurringExpenseDto(RecurringExpense recurringExpense) {
        super(recurringExpense);
        lastOccurence = recurringExpense.getLastOccurence();
        recurrence = recurringExpense.getRecurrence();
        expirationDate = recurringExpense.getExpirationDate();
    }

    public LocalDate getLastOccurence() {
        return lastOccurence;
    }

    public Period getRecurrence() {
        return recurrence;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(super.toString());
        sb.append("Expense recurrence: ");
        sb.append(recurrence);
        sb.append("\n");
        sb.append("Expires on: ");
        sb.append(expirationDate);
        sb.append("\n");
        return sb.toString();
    }
}

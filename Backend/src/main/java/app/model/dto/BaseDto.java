package app.model.dto;

import java.util.Objects;

import app.model.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonCreator;

public abstract class BaseDto {

    private final long id;

    public BaseDto() {
        id = 0;
    }

    public BaseDto(long id) {
        this.id = id;
    }

    public BaseDto(BaseEntity baseEntity) {
        id = baseEntity.getId();
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        BaseDto baseDto = (BaseDto) o;
        return id == baseDto.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

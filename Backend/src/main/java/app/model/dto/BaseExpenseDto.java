package app.model.dto;

import app.model.entity.BaseExpense;

public abstract class BaseExpenseDto extends BaseDto {

    private final long budgetId;
    private final String description;
    private final double amount;

    public BaseExpenseDto() {
        budgetId = 0;
        description = "";
        amount = 0;
    }

    public BaseExpenseDto(long id, long budgetId, String description, double amount) {
        super(id);
        this.budgetId = budgetId;
        this.description = description;
        this.amount = amount;
    }

    public BaseExpenseDto(BaseExpense baseExpense) {
        super(baseExpense);
        budgetId = baseExpense.getBudget().getId();
        description = baseExpense.getDescription();
        amount = baseExpense.getAmount();
    }

    public long getBudgetId() {
        return budgetId;
    }

    public String getDescription() {
        return description;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return description + "\n" + "Amount: " + amount + "\n";
    }
}

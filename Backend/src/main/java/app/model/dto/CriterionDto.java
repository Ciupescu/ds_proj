package app.model.dto;

import app.criteria.ExpenseCriterion;

public class CriterionDto {

    private ExpenseCriterion expenseCriterion;
    private String param;

    public ExpenseCriterion getExpenseCriterion() {
        return expenseCriterion;
    }

    public void setExpenseCriterion(ExpenseCriterion expenseCriterion) {
        this.expenseCriterion = expenseCriterion;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }
}

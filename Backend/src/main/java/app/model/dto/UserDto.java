package app.model.dto;

import app.model.entity.User;

public class UserDto extends BaseDto {

    private final byte[] username;
    private final byte[] password;

    public UserDto() {
        username = new byte[]{};
        password = new byte[]{};
    }

    public UserDto(long id, byte[] username, byte[] password) {
        super(id);
        this.username = username;
        this.password = password;
    }

    public UserDto(User user) {
        super(user);
        username = user.getEmail();
        password = user.getPassword();
    }

    public byte[] getUsername() {
        return username;
    }

    public byte[] getPassword() {
        return password;
    }

}

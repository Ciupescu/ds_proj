package app.model.dto;

public class MailContextDto {

    private final String to;
    private final String subject;
    private final String content;

    public MailContextDto() {
        to = "ciucescuv@outlook.com";
        subject = "spam";
        content = "spam";
    }

    public MailContextDto(String to, String subject, String content) {
        this.to = to;
        this.subject = subject;
        this.content = content;
    }

    public String getTo() {
        return to;
    }

    public String getSubject() {
        return subject;
    }

    public String getContent() {
        return content;
    }
}

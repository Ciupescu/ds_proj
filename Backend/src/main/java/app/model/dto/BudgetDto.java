package app.model.dto;

import app.model.Period;
import app.model.entity.Budget;

import java.time.LocalDate;

public class BudgetDto extends BaseDto {

    private final String currency;
    private final double amount;
    private final long categoryId;
    private final String description;
    private final Period valability;
    private final double amountConsumed;
    private final LocalDate lastRenewal;

    public BudgetDto() {
        currency = "RON";
        amount = 0;
        categoryId = 0;
        description = "";
        valability = Period.DAILY;
        amountConsumed = 0;
        lastRenewal = LocalDate.now();
    }

    public BudgetDto(long id, String currency, double amount, long categoryId, String description, Period valability, double amountConsumed, LocalDate lastRenewal) {
        super(id);
        this.currency = currency;
        this.amount = amount;
        this.categoryId = categoryId;
        this.description = description;
        this.valability = valability;
        this.amountConsumed = amountConsumed;
        this.lastRenewal = lastRenewal;
    }

    public BudgetDto(Budget budget) {
        super(budget);
        currency = budget.getCurrency().getSymbol();
        amount = budget.getAmount();
        categoryId = budget.getCategory().getId();
        description = budget.getDescription();
        valability = budget.getValability();
        amountConsumed = budget.getAmountConsumed();
        lastRenewal = budget.getLastRenewal();
    }

    public String getCurrency() {
        return currency;
    }

    public double getAmount() {
        return amount;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public String getDescription() {
        return description;
    }

    public Period getValability() {
        return valability;
    }

    public double getAmountConsumed() {
        return amountConsumed;
    }

    public LocalDate getLastRenewal() {
        return lastRenewal;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Budget: ");
        sb.append(description);
        sb.append("\n");
        sb.append("Amount: ");
        sb.append(amount);
        sb.append(currency);
        sb.append("\n");
        sb.append("Valability: ");
        sb.append(valability);
        sb.append("\n");
        return sb.toString();
    }
}

package app.model;

import java.util.HashMap;
import java.util.Map;

public enum Currency {

    DOLLAR("$"),
    EURO("€"),
    LEU("RON");

    private static final Map<String, Currency> lookup = new HashMap<>();
    static{
        for(Currency c:Currency.values()) {
            lookup.put(c.getSymbol(), c);
        }
    }

    public static Currency get(String symbol){
        return lookup.get(symbol);
    }

    private String symbol;

    Currency(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return this.symbol;
    }


}

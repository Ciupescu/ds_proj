package app.model;

public enum BaseCategory {

    FOOD,
    CLOTHES,
    SUBSCRIPTIONS,
    HEALTH,
    TAXES,
    OTHERS
}

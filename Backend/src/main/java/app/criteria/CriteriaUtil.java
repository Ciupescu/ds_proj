package app.criteria;

import app.model.entity.ExpenseLog;
import app.model.Currency;
import app.model.dto.CriterionDto;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDateTime;
import java.util.List;

public abstract class CriteriaUtil {

    private CriteriaUtil() {}

    private static Specification<ExpenseLog> budgetEqualTo(long budgetId) {
        return (expenseLog, cq, cb) -> cb.equal(expenseLog.get("budget").get("id"), budgetId);
    }

    private static Specification<ExpenseLog> hasCurrency(Currency currency) {
        return (expenseLog, cq, cb) -> cb.equal(expenseLog.get("budget").get("currency"), currency);
    }

    private static Specification<ExpenseLog> amountGreaterThan(double amount) {
        return (expenseLog, cq, cb) -> cb.greaterThan(expenseLog.get("amount"), amount);
    }

    private static Specification<ExpenseLog> amountLessThan(double amount) {
        return (expenseLog, cq, cb) -> cb.lessThan(expenseLog.get("amount"), amount);
    }

    private static Specification<ExpenseLog> amountEqualTo(double amount) {
        return (expenseLog, cq, cb) -> cb.equal(expenseLog.get("amount"), amount);
    }

    private static Specification<ExpenseLog> dateBefore(LocalDateTime date) {
        return (expenseLog, cq, cb) -> cb.lessThan(expenseLog.get("timestamp"), date);
    }

    private static Specification<ExpenseLog> dateAfter(LocalDateTime date) {
        return (expenseLog, cq, cb) -> cb.greaterThan(expenseLog.get("timestamp"), date);
    }

    private static Specification<ExpenseLog> dateEqualTo(LocalDateTime date) {
        return (expenseLog, cq, cb) -> cb.equal(expenseLog.get("timestamp"), date);
    }

    private static Specification<ExpenseLog> getCriterionSpecification(CriterionDto criterion) {
        switch (criterion.getExpenseCriterion()) {
            case BUDGET_CRITERION:
                long budgetId = Long.parseLong(criterion.getParam());
                return budgetEqualTo(budgetId);
            case AMOUNT_GREATER_CRITERION:
                double amount1 = Double.parseDouble(criterion.getParam());
                return amountGreaterThan(amount1);
            case AMOUNT_LESS_CRITERION:
                double amount2 = Double.parseDouble(criterion.getParam());
                return amountLessThan(amount2);
            case AMOUNT_EQUAL_CRITERION:
                double amount3 = Double.parseDouble(criterion.getParam());
                return amountEqualTo(amount3);
            case DATE_BEFORE_CRITERION:
                LocalDateTime timestamp1 = LocalDateTime.parse(criterion.getParam());
                return dateBefore(timestamp1);
            case DATE_EQUAL_CRITERION:
                LocalDateTime timestamp2 = LocalDateTime.parse(criterion.getParam());
                return dateEqualTo(timestamp2);
            case DATE_AFTER_CRITERION:
                LocalDateTime timestamp3 = LocalDateTime.parse(criterion.getParam());
                return dateAfter(timestamp3);
            case CURRENCY_CRITERION:
                Currency currency = Currency.valueOf(criterion.getParam());
                return hasCurrency(currency);
            default:
                return (expenseLog, cq, cb) -> cb.conjunction();
        }
    }

    public static Specification<ExpenseLog> getCriteriaSpecification(List<CriterionDto> criteria) {
        Specification<ExpenseLog> specification = (expenseLog, cq, cb) -> cb.conjunction();

        for (CriterionDto criterion : criteria) {
            specification = specification.and(getCriterionSpecification(criterion));
        }
        return specification;
    }

}

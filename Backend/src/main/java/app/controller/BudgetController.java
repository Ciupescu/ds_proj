package app.controller;

import app.model.dto.BudgetDto;
import app.service.BudgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/budgets")
public class BudgetController {

    private BudgetService budgetService;

    public BudgetController() {
    }

    @Autowired
    public BudgetController(BudgetService budgetService) {
        this.budgetService = budgetService;
    }

    @GetMapping(path = "{budgetId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public BudgetDto getBudget(@PathVariable("budgetId") long budgetId) {
        return budgetService.getBudgetById(budgetId);
    }

    @GetMapping(path = "/for-category/{categoryId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<BudgetDto> getBudgetsCategory(@PathVariable("categoryId") long categoryId) {
        return budgetService.getBudgetsCategory(categoryId);
    }

    @GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<BudgetDto> getBudgets() {
        return budgetService.getBudgets();
    }

    @PostMapping(path = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public long addBudget(@RequestBody BudgetDto budgetDto) {
        return budgetService.addBudget(budgetDto);
    }

    @PutMapping("/update")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateBudget(@RequestBody BudgetDto budgetDto) {
        budgetService.updateBudget(budgetDto);
    }

    @DeleteMapping("/remove")
    @ResponseStatus(HttpStatus.CREATED)
    public void removeBudget(@RequestParam long budgetId) {
        budgetService.removeBudget(budgetId);
    }

    @PostMapping("/enable-mail-notifications")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void enableMailNotifications(@RequestBody boolean enabled) {
        budgetService.enableMailNotifications(enabled);
    }
}

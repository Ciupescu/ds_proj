package app.controller;

import app.model.dto.CriterionDto;
import app.model.dto.ExpenseLogDto;
import app.model.dto.MailContextDto;
import app.service.ExpenseLogService;
import app.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/management")
public class ManagementController {

    private MailService mailService;
    private ExpenseLogService expenseLogService;

    public ManagementController() {}

    @Autowired
    public ManagementController(MailService mailService, ExpenseLogService expenseLogService) {
        this.mailService = mailService;
        this.expenseLogService = expenseLogService;
    }

    @PostMapping("/mail")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void sendMail(@RequestBody MailContextDto mailContextDto) {
        mailService.sendMail(mailContextDto);
    }

    @PostMapping(path = "/filter", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<ExpenseLogDto> filterExpenses(@RequestBody List<CriterionDto> criteria){
        return expenseLogService.filterExpenses(criteria);
    }
}

package app.controller;

import app.model.dto.RecurringExpenseDto;
import app.service.RecurringExpenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/recurring")
public class RecurringExpenseController {

    private RecurringExpenseService recurringExpenseService;

    public RecurringExpenseController() {
    }

    @Autowired
    public RecurringExpenseController(RecurringExpenseService recurringExpenseService) {
        this.recurringExpenseService = recurringExpenseService;
    }

    @GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<RecurringExpenseDto> getRecurringExpensesBudget(@RequestParam("budgetId") long budgetId) {
        return recurringExpenseService.getRecurringExpensesBudget(budgetId);
    }


    @GetMapping(path = "/all/valid", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<RecurringExpenseDto> getRecurringExpensesValid(@RequestParam("budgetId") long budgetId) {
        return recurringExpenseService.getRecurringExpensesValid(budgetId);
    }

    @PostMapping(path = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public long addRecurringExpense(@RequestBody RecurringExpenseDto recurringExpenseDto) {
        return recurringExpenseService.addRecurringExpense(recurringExpenseDto);
    }

    @PutMapping("/update")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateRecurringExpense(@RequestBody RecurringExpenseDto recurringExpenseDto) {
        recurringExpenseService.updateRecurringExpense(recurringExpenseDto);
    }

    @DeleteMapping("/remove")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeRecurringExpense(@RequestParam long recurringExpenseId) {
        recurringExpenseService.removeRecurringExpense(recurringExpenseId);
    }

    @PostMapping("/enable-mail-notifications")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void enableMailNotifications(@RequestBody boolean enabled) {
        recurringExpenseService.enableMailNotifications(enabled);
    }
}

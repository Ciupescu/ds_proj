package app.controller;

import app.model.dto.ExpenseLogDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import app.service.ExpenseLogService;

import java.util.List;

@RestController
@RequestMapping("/logs")
public class ExpenseLogController {

    private ExpenseLogService expenseLogService;

    public ExpenseLogController() {}

    @Autowired
    public ExpenseLogController(ExpenseLogService expenseLogService) {
        this.expenseLogService = expenseLogService;
    }

    @GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<ExpenseLogDto> getExpenseLogsBudget(@RequestParam("budgetId")long budgetId){
        return expenseLogService.getExpenseLogsBudget(budgetId);
    }

    @PostMapping(path = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public long addExpenseLog(@RequestBody ExpenseLogDto expenseLogDto) {
        return expenseLogService.addExpenseLog(expenseLogDto);
    }

    @DeleteMapping("/remove")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public  void removeExpenseLog(@RequestParam long expenseLogId) {
        expenseLogService.deleteExpenseLog(expenseLogId);
    }
}

package app.repository;

import app.model.BaseCategory;
import app.model.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    List<Category> findAllByBaseCategory(BaseCategory baseCategory);
}

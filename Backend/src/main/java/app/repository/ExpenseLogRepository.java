package app.repository;

import app.model.entity.ExpenseLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExpenseLogRepository extends JpaRepository<ExpenseLog, Long>, JpaSpecificationExecutor<ExpenseLog> {

    List<ExpenseLog> findAllByBudgetId(long budgetId);
}

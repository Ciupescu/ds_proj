package app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Cannot delete category which is assigned to budgets.")
public class CategoryDeleteException extends RuntimeException {
}

package app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Could not find given user.")
public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException() {
        super("Could not find given user.");
    }
}
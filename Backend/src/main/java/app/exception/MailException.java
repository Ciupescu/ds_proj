package app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Cannot send mail.")
public class MailException extends RuntimeException {

    public MailException(Exception cause) {
        super("Cannot send mail: " + cause.getMessage());
    }
}

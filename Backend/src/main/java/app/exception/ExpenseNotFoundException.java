package app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Could not find given expense.")
public class ExpenseNotFoundException extends RuntimeException {

    public ExpenseNotFoundException() {
        super("Could not find given expense.");
    }
}
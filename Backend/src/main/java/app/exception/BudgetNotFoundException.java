package app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Could not find given budget.")
public class BudgetNotFoundException extends RuntimeException {

    public BudgetNotFoundException() {
        super("Could not find given budget.");
    }
}


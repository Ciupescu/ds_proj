package app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Could not find given category.")
public class CategoryNotFoundException extends RuntimeException {

    public CategoryNotFoundException() {
        super("Could not find given category.");
    }
}

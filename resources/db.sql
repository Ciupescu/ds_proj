-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: budget-db
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `base_expenses`
--

DROP TABLE IF EXISTS `base_expenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_expenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `budget_id` int(11) NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `budget_fk_idx` (`budget_id`),
  CONSTRAINT `budget_fk` FOREIGN KEY (`budget_id`) REFERENCES `budgets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_expenses`
--

LOCK TABLES `base_expenses` WRITE;
/*!40000 ALTER TABLE `base_expenses` DISABLE KEYS */;
INSERT INTO `base_expenses` VALUES (1,1,'bread',2.99),(2,1,'milk',4.5),(3,2,'mcChicken',5.6),(4,3,'leather shoes',100),(5,3,'shiny shoes',200),(6,3,'black shoes',300),(7,4,'spotify subscription',5),(8,5,'phone bill',34.5),(9,6,'aspirin',18),(10,6,'aspirin Bayer',22),(11,6,'xanax',10),(12,6,'cheap aspirin',11),(13,6,'cheap aspirin ',10),(14,7,'insurance',10),(15,8,'electricity',57.6),(16,9,'cheltuieli de bloc',333),(17,10,'cat food',22.3),(18,10,'toys',72.2),(19,10,'litter',40.2),(20,10,'vet',50),(21,11,'new bracket',200),(22,11,'dentist visit',100),(23,12,'new phone',1800),(24,13,'foie gras',555),(25,13,'sushi',333.5),(26,14,'vanilla cola',3.4),(27,14,'paprika chips',6.7),(28,14,'cinnamon cola',3.4),(29,14,'salt chips',4.4),(30,14,'cola zero',3.2),(31,15,'pizza order',20),(32,1,'bread',2.99),(33,10,'litter',40.2),(35,1,'bread',2.99),(36,10,'litter',40.2);
/*!40000 ALTER TABLE `base_expenses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `budgets`
--

DROP TABLE IF EXISTS `budgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `budgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valability` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_consumed` double NOT NULL,
  `last_renewal` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `category_fk_idx` (`category_id`),
  CONSTRAINT `category_fk` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `budgets`
--

LOCK TABLES `budgets` WRITE;
/*!40000 ALTER TABLE `budgets` DISABLE KEYS */;
INSERT INTO `budgets` VALUES (1,'DOLLAR',1000,1,'budget for everyday items','DAILY',0,'2018-12-05'),(2,'EURO',999.5,2,'budget for McDonalds','MONTHLY',0,'2018-12-04'),(3,'LEU',100,3,'budget for nice shoes','YEARLY',100,'2018-03-10'),(4,'DOLLAR',500,4,'budget for Spotify','MONTHLY',0,'2018-12-04'),(5,'EURO',499.99,5,'DigiMobil budget','MONTHLY',0,'2018-12-04'),(6,'LEU',2000,6,'Aspirin and Xanax budget','WEEKLY',0,'2018-12-04'),(7,'DOLLAR',300,7,'Regina Maria insurance and expenses budget','MONTHLY',0,'2018-12-04'),(8,'EURO',500.4,8,'Budget for electricity and gas','MONTHLY',0,'2018-12-04'),(9,'LEU',2000,8,'Budget for cheltuieli de bloc','MONTHLY',0,'2018-12-04'),(10,'DOLLAR',150,9,'Budget for Pipi','WEEKLY',0,'2018-12-04'),(11,'EURO',175,10,'Budget for monthly dentist visits and occasional bracket changes','MONTHLY',160,'2018-11-13'),(12,'LEU',1750,11,'Yearly budget for buying phones','YEARLY',1700,'2017-12-07'),(13,'DOLLAR',10000.1,1,'Fancy food budget','MONTHLY',0,'2018-12-04'),(14,'EURO',10,2,'Budget for chips and soda','DAILY',0,'2018-12-05'),(15,'LEU',200,1,'Home order budget','WEEKLY',0,'2018-12-04');
/*!40000 ALTER TABLE `budgets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `base_category` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'BASIC STUFF','FOOD','items such as bread, milk etc.'),(2,'FAST FOOD','FOOD','trash food'),(3,'FANCY CLOTHES','CLOTHES','such as suits etc'),(4,'MUSIC','SUBSCRIPTIONS',' '),(5,'COMMUNICATIONS','SUBSCRIPTIONS','such as internet or mobile'),(6,'DRUGS','HEALTH',' '),(7,'INSURANCE','HEALTH',' '),(8,'UTILITIES','TAXES','such as electricity'),(9,'CAT STUFF','OTHERS','expenses for the cat such as food'),(10,'DENTIST','HEALTH',' '),(11,'ELECTRONICS','OTHERS','mobile phone, printer etc.');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expense_logs`
--

DROP TABLE IF EXISTS `expense_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expense_logs` (
  `id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  CONSTRAINT `base_expense_fk` FOREIGN KEY (`id`) REFERENCES `base_expenses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expense_logs`
--

LOCK TABLES `expense_logs` WRITE;
/*!40000 ALTER TABLE `expense_logs` DISABLE KEYS */;
INSERT INTO `expense_logs` VALUES (2,'2018-12-02 10:00:00'),(3,'2017-11-04 11:00:00'),(4,'2016-10-06 08:00:00'),(5,'2017-09-08 12:00:00'),(6,'2018-08-10 07:00:00'),(9,'2017-07-12 13:00:00'),(10,'2016-06-14 06:00:00'),(11,'2017-05-16 14:00:00'),(12,'2018-04-18 05:00:00'),(13,'2017-03-20 16:00:00'),(15,'2016-02-22 05:00:00'),(16,'2015-01-24 17:00:00'),(18,'2016-02-26 04:00:00'),(20,'2017-03-28 17:00:00'),(21,'2018-04-30 02:00:00'),(23,'2017-05-29 18:00:00'),(24,'2016-06-27 01:00:00'),(25,'2015-07-25 19:00:00'),(26,'2016-08-23 00:00:00'),(27,'2017-09-21 20:00:00'),(28,'2018-10-18 23:00:00'),(29,'2017-11-16 22:00:00'),(30,'2016-12-14 23:00:00'),(31,'2015-11-13 10:00:00'),(32,'2018-12-03 22:00:00'),(33,'2018-12-03 22:00:01'),(35,'2018-12-05 09:08:02'),(36,'2018-12-05 09:08:02');
/*!40000 ALTER TABLE `expense_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recurring_expenses`
--

DROP TABLE IF EXISTS `recurring_expenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recurring_expenses` (
  `id` int(11) NOT NULL,
  `last_occurence` date NOT NULL,
  `recurrence` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration_date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  CONSTRAINT `base_expense_fk2` FOREIGN KEY (`id`) REFERENCES `base_expenses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recurring_expenses`
--

LOCK TABLES `recurring_expenses` WRITE;
/*!40000 ALTER TABLE `recurring_expenses` DISABLE KEYS */;
INSERT INTO `recurring_expenses` VALUES (1,'2018-12-05','DAILY','2019-12-31'),(7,'2018-12-03','MONTHLY','2019-04-03'),(8,'2018-11-11','MONTHLY','2019-12-12'),(14,'2018-09-09','MONTHLY','2018-09-09'),(17,'2018-12-12','WEEKLY','2019-12-12'),(19,'2018-12-05','DAILY','2018-12-24'),(22,'2018-11-11','MONTHLY','2019-12-31');
/*!40000 ALTER TABLE `recurring_expenses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varbinary(100) NOT NULL,
  `password` varbinary(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `password_UNIQUE` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-05 20:00:13

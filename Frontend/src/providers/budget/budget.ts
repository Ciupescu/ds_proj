import {Injectable} from '@angular/core';
import {Budget} from "../../domain/budget";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class BudgetProvider {

    apiUrl: string = 'http://192.168.43.69:8080/budgets';

    constructor(private http: HttpClient) {
    }

    getBudgets() {
        return this.http.get<Budget[]>(this.apiUrl + '/all');
    }

    enableNotifications(allowNotifications: boolean) {
        return this.http.post(this.apiUrl + '/enable-mail-notifications', JSON.stringify(allowNotifications), {
            headers: {'Content-Type': 'application/json'}
        });
    }

    updateBudget(budget: Budget) {
        return this.http.put(this.apiUrl + '/update', JSON.stringify(budget), {
            headers: {'Content-Type': 'application/json'}
        })
    }

    deleteBudget(budget: Budget) {
        return this.http.delete(this.apiUrl + '/remove', {
            headers: {'Content-Type': 'application/json'},
            params: {'budgetId': JSON.stringify(budget.id)}
        });
    }

    addBudget(budget: Budget) {
        return this.http.post(this.apiUrl + '/add', JSON.stringify(budget), {
            headers: {'Content-Type': 'application/json'}
        })
    }
}

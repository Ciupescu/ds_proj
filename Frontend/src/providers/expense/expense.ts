import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ExpenseLog} from "../../domain/expenseLog";
import {RecurringExpense} from "../../domain/recurringExpense";

@Injectable()
export class ExpenseProvider {

    logsApiUrl: string = 'http://192.168.43.69:8080/logs';
    recurringApiUrl: string = 'http://localhost:8080/recurring';

    constructor(public http: HttpClient) {
    }

    getExpensesLogs(budgetId: number) {
        return this.http.get<ExpenseLog[]>(this.logsApiUrl + '/all', {params: {budgetId: budgetId.toString()}})
    }

    getRecurringExpenses(budgetId: number) {
        return this.http.get<RecurringExpense[]>(this.recurringApiUrl + '/all', {params: {budgetId: budgetId.toString()}});
    }

    getValidRecurringExpenses(budgetId: number) {
        return this.http.get<RecurringExpense[]>(this.recurringApiUrl + '/all/valid', {params: {budgetId: budgetId.toString()}});
    }

    enableNotifications(allowNotifications: boolean) {
        return this.http.post(this.recurringApiUrl + '/enable-mail-notifications', JSON.stringify(allowNotifications), {
            headers: {'Content-Type': 'application/json'}
        });
    }

    addExpenseLog(expense: ExpenseLog) {
        return this.http.post(this.logsApiUrl + '/add', JSON.stringify(expense), {
            headers: {'Content-Type': 'application/json'}
        })
    }

    deleteExpenseLog(expenseLog: ExpenseLog) {
        return this.http.delete(this.logsApiUrl + '/remove', {
            headers: {'Content-Type': 'application/json'},
            params: {'expenseLogId': JSON.stringify(expenseLog.id)}
        })
    }

    addRecurringExpense(expense: RecurringExpense) {
        return this.http.post(this.recurringApiUrl + '/add', JSON.stringify(expense), {
            headers: {'Content-Type': 'application/json'}
        })
    }

    deleteRecurringExpense(recExpense: RecurringExpense) {
        return this.http.delete(this.recurringApiUrl + '/remove', {
            headers: {'Content-Type': 'application/json'},
            params: {'recurringExpenseId': JSON.stringify(recExpense.id)}
        })
    }

    updateRecurringExpense(recExpense: RecurringExpense) {
        return this.http.put(this.recurringApiUrl + '/update', JSON.stringify(recExpense), {
            headers: {'Content-Type': 'application/json'}
        })
    }
}

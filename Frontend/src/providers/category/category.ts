import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Category} from "../../domain/category";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from "rxjs";

@Injectable()
export class CategoryProvider {

    apiUrl: string = "http://192.168.43.69:8080/categories";

    constructor(public http: HttpClient) {
    }

    getBaseCategories() {
        return this.http.get<string[]>(this.apiUrl + "/base");
    }

    getSubcategories(baseCategory: string) {
        return this.http.get<Category[]>(this.apiUrl + '/forBase', {params: {baseCateg: baseCategory}});
    }

    getCategoryForId(categoryId: number) {
        return this.http.get<Category>(this.apiUrl + '/', {params: {categoryId: categoryId.toString()}});
    }

    getAllSubcategories() {
        return this.http.get<Category[]>(this.apiUrl + '/all');
    }

    deleteCategory(categ: Category): Observable<any> {
        return this.http.delete(this.apiUrl + '/remove', {
            headers: {'Content-Type': 'application/json'},
            params: {'categId': JSON.stringify(categ.id)}
        }).map(response => JSON.stringify(response))
            .catch((e:any) => {
                if (e.status == 409) {
                    return Observable.throw(new Error("Cannot delete category which is assigned to budgets"))
                }
            })
    }

    updateCategory(categ: Category) {
        return this.http.put(this.apiUrl + '/update', JSON.stringify(categ), {
            headers: {'Content-Type': 'application/json'}
        })
    }

    addCategory(categ: Category) {
        return this.http.post(this.apiUrl + '/add', JSON.stringify(categ), {
            headers: {'Content-Type': 'application/json'}
        })
    }
}

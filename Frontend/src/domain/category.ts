export class Category {
    id: number;
    name: string;
    description: string;
    baseCategory: string;
}
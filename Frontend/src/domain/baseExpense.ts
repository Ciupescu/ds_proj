export class BaseExpense{
    id: number;
    budgetId: number;
    description: string;
    amount: number;
}
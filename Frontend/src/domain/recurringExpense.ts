import {BaseExpense} from "./baseExpense";
import {Period} from "./period";

export class RecurringExpense extends BaseExpense{
    lastOccurence: Date;
    recurrence: Period;
    expirationDate: Date;
}
import {Period} from "./period";
import {Currency} from "./currency";

export class Budget {
    id: number;
    currency: Currency;
    amount: number;
    categoryId: number;
    description: string;
    valability: Period;
    amountConsumed: number;
    lastRenewal: Date;
}
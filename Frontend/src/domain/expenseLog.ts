import {BaseExpense} from "./baseExpense";

export class ExpenseLog extends BaseExpense {
    timestamp: string;
}
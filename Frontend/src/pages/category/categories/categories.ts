import {Component} from '@angular/core';
import {AlertController, Events, NavController, ToastController} from 'ionic-angular';
import {Category} from "../../../domain/category";
import {CategoryProvider} from "../../../providers/category/category";
import {CategoryAddPage} from "../category-add/category-add";
import {CategoryEditPage} from "../category-edit/category-edit";

@Component({
    selector: 'page-categories',
    templateUrl: 'categories.html'
})
export class CategoriesPage {

    baseCategories: string[] = [];
    baseCategoriesData: Array<{ categ: string, icon: string, showDetails: boolean, subcategories: Category[] }> = [];

    constructor(public navCtrl: NavController, private categoryProvider: CategoryProvider, private alertCtrl: AlertController, private toastCtrl: ToastController, private events: Events) {
        events.subscribe('addedCategory', (categ) => this.addCategData(categ));
        events.subscribe('updatedCategory', (categ) => this.updateCategData(categ));
        this.categoryProvider.getBaseCategories().subscribe(data => {
            this.baseCategories = data;
            this.baseCategories.forEach(bc => {
                this.categoryProvider.getSubcategories(bc).subscribe(subcategories => {
                    this.baseCategoriesData.push({categ: bc, icon: 'md-arrow-down', showDetails: false, subcategories})
                });
            });
        });

    }

    toggleDetails(bc) {
        if (bc.showDetails) {
            bc.showDetails = false;
            bc.icon = 'md-arrow-down';
        } else {
            bc.showDetails = true;
            bc.icon = 'md-arrow-up';
        }
    }

    addCategory(): void {
        this.navCtrl.push(CategoryAddPage);
    }

    editCategory(categ: Category): void {
        this.navCtrl.push(CategoryEditPage, {'categ':categ} );
    }

    private deleteCategoryPrompt(categ: Category) {
        let alert = this.alertCtrl.create({
            title: "Delete Category",
            message: "Really delete category?",
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'Delete',
                    handler: () => this.deleteCategory(categ)
                }
            ]
        });
        alert.present();
    }

    private deleteCategory(categ: Category) {
        this.categoryProvider.deleteCategory(categ).subscribe(value => {
            let toast = this.toastCtrl.create({
                message: 'Deleted',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
            const baseCat = categ.baseCategory;
            const baseCatData = this.baseCategoriesData.find(data => data.categ == baseCat);
            const index = baseCatData.subcategories.findIndex(subCat => subCat.id == categ.id);
            baseCatData.subcategories.splice(index, 1);
        }, err => {
            let toast = this.toastCtrl.create({
                message: err,
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    }

    private addCategData(categ: Category) {
        const baseCat = categ.baseCategory;
        const baseCatData = this.baseCategoriesData.find(data => data.categ == baseCat);
        baseCatData.subcategories.push(categ);
    }

    private updateCategData(categ: Category) {
        const baseCat = categ.baseCategory;
        const baseCatData = this.baseCategoriesData.find(data => data.categ == baseCat);
        const index = baseCatData.subcategories.findIndex(subCat => subCat.id == categ.id);
        baseCatData.subcategories.splice(index, 1);
        baseCatData.subcategories.push(categ);
    }
}

import {Component} from '@angular/core';
import {Events, NavController, NavParams, ToastController} from 'ionic-angular';
import {Category} from "../../../domain/category";
import {CategoryProvider} from "../../../providers/category/category";

@Component({
    selector: 'page-category-edit',
    templateUrl: 'category-edit.html',
})
export class CategoryEditPage {

    category: Category;
    newName: string;
    newDescription: string;

    constructor(public navCtrl: NavController, public navParams: NavParams, private categoryProvider: CategoryProvider, private toastCtrl: ToastController, private events: Events) {
        this.category = navParams.get("categ");
    }

    private updateCategory() {
        if (this.newDescription && this.newDescription.length != 0) {
            this.category.description = this.newDescription;
        }
        if (this.newName && this.newName.length != 0) {
            this.category.name = this.newName;
        }
        this.categoryProvider.updateCategory(this.category).subscribe(value => {
            let toast = this.toastCtrl.create({
                message: 'Updated category',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
            this.events.publish('updatedCategory', this.category);
        }, err => {
            let toast = this.toastCtrl.create({
                message: 'Could not update',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    }

}

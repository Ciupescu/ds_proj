import {Component} from '@angular/core';
import {Events, NavController, NavParams, ToastController} from 'ionic-angular';
import {CategoryProvider} from "../../../providers/category/category";
import {Category} from "../../../domain/category";

@Component({
    selector: 'page-category-add',
    templateUrl: 'category-add.html',
})
export class CategoryAddPage {

    newName: string;
    newDescription: string;
    newBaseCategory: string;
    baseCategories: string[] = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, private categoryProvider: CategoryProvider, private toastCtrl: ToastController, private events: Events) {
        this.categoryProvider.getBaseCategories().subscribe(data => this.baseCategories = data);
    }

    private toast(message: string) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }

    private compareBaseCategories(c1: string, c2: string): boolean {
        return c1 === c2;
    }

    private addCategory() {
        if (!this.newName || this.newName.length === 0) {
            this.toast('Invalid name');
            return;
        }
        if (!this.newDescription || this.newDescription.length === 0) {
            this.toast('Invalid description');
            return;
        }
        if (!this.newBaseCategory || this.newBaseCategory.length === 0) {
            this.toast('Invalid baseCategory');
            return;
        }
        const categ: Category = {
            id: 0,
            name: this.newName,
            description: this.newDescription,
            baseCategory: this.newBaseCategory
        };
        this.categoryProvider.addCategory(categ).subscribe(value => {
            let toast = this.toastCtrl.create({
                message: 'Added new category',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
            categ.id = Number(value);
            this.events.publish('addedCategory', categ);
        }, err => {
            let toast = this.toastCtrl.create({
                message: 'Could not add new category',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    }
}

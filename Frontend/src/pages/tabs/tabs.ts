import { Component } from '@angular/core';

import { BudgetsPage } from '../budget/budgets/budgets';
import { CategoriesPage } from '../category/categories/categories';
import { ManagementPage } from '../management/management';
import {ExpensesPage} from "../expense/expenses/expenses";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = BudgetsPage;
  tab2Root = CategoriesPage;
  tab3Root = ExpensesPage;
  tab4Root = ManagementPage;

  constructor() {

  }
}

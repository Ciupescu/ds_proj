import {Component} from '@angular/core';
import {NavController, ToastController} from 'ionic-angular';
import {TabsPage} from "../tabs/tabs";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  username: string;
  password: string;

  constructor(public navCtrl: NavController, private toastCtrl: ToastController) {
  }

  logIn(): void {
    if (!this.username || this.username.length === 0) {
      this.errorToaster("Enter a username");
      return;
    }
    if (!this.password || this.password.length === 0) {
      this.errorToaster("Enter a password");
      return;
    }
    if (this.username != "user" || this.password != "pass") {
      this.errorToaster("Invalid username or password");
      return;
    }
    this.navCtrl.push(TabsPage);
  }

  errorToaster(message: string): void {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

}

import {Component} from '@angular/core';
import {NavController, ToastController} from 'ionic-angular';
import {Budget} from "../../../domain/budget";
import {Category} from "../../../domain/category";
import {Period} from "../../../domain/period";
import {BudgetProvider} from "../../../providers/budget/budget";
import {CategoryProvider} from "../../../providers/category/category";
import {Currency} from "../../../domain/currency";
import {Events} from "ionic-angular";

@Component({
    selector: 'page-budget-add',
    templateUrl: 'budget-add.html',
})
export class BudgetAddPage {

    newCategory: Category;
    newAmount: number;
    newDescription: string;
    newValability: Period;
    newCurrency: Currency;
    categories: Category[] = [];
    valabilities: Period[] = [];
    currencies: Currency[] = [];

    constructor(public navCtrl: NavController, private budgetProvider: BudgetProvider, private categoryProvider: CategoryProvider, private toastCtrl: ToastController, private events: Events) {
        this.valabilities.push(Period.DAILY);
        this.valabilities.push(Period.WEEKLY);
        this.valabilities.push(Period.MONTHLY);
        this.valabilities.push(Period.YEARLY);
        this.currencies.push(Currency.DOLLAR);
        this.currencies.push(Currency.EURO);
        this.currencies.push(Currency.LEU);
        categoryProvider.getAllSubcategories().subscribe(data => this.categories = data);
    }

    private toast(message: string) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }

    private addBudget() {
        if (!this.newAmount || Number.isNaN(this.newAmount)) {
            this.toast('Invalid amount');
            return;
        }
        if (!this.newDescription || this.newDescription.length === 0) {
            this.toast('Invalid description');
            return;
        }
        if (!this.newCategory) {
            this.toast('Invalid category');
            return;
        }
        if (!this.newValability) {
            this.toast('Invalid valability');
            return;
        }
        if (!this.newCurrency) {
            this.toast('Invalid currency');
            return;
        }
        const budget: Budget = {id: 0,
        currency: this.newCurrency,
        amount: this.newAmount,
        categoryId: this.newCategory.id,
        description: this.newDescription,
        valability: this.newValability,
        amountConsumed: 0,
        lastRenewal: new Date()};
        this.budgetProvider.addBudget(budget).subscribe(value => {
            let toast = this.toastCtrl.create({
                message: 'Added new budget',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
            budget.id = Number(value);
            this.events.publish('addedBudget', budget, this.newCategory);
        }, err => {
            let toast = this.toastCtrl.create({
                message: 'Could not add new budget',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    }

    private compareCategories(c1: Category, c2: Category): boolean {
        return c1 && c2 ? c1.id === c2.id : c1 === c2;
    }

    private compareValabilities(v1: Period, v2: Period): boolean {
        return  v1 === v2;
    }

    private compareCurrencies(c1: Currency, c2: Currency): boolean {
        return c1 === c2;
    }
}

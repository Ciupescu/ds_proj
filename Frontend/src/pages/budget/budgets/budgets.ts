import {Component} from '@angular/core';
import {AlertController, NavController, ToastController} from 'ionic-angular';
import {BudgetProvider} from "../../../providers/budget/budget";
import {Budget} from "../../../domain/budget";
import {BudgetEditPage} from "../budget-edit/budget-edit";
import {BudgetAddPage} from "../budget-add/budget-add";
import {Category} from "../../../domain/category";
import {CategoryProvider} from "../../../providers/category/category";
import {Events} from "ionic-angular";
import {ExpenseLog} from "../../../domain/expenseLog";

@Component({
    selector: 'page-budgets',
    templateUrl: 'budgets.html'
})
export class BudgetsPage {

    private budgetData: Array<{ budget: Budget, icon: string, showDetails: boolean, categ: Category }> = [];
    private allowNotifications: boolean = true;

    constructor(public navCtrl: NavController, private budgetProvider: BudgetProvider, private categoryProvider: CategoryProvider, private alertCtrl: AlertController, private toastCtrl: ToastController, private events: Events) {
        events.subscribe('addedBudget', (budget, categ) => this.addBudgetData(budget, categ));
        events.subscribe('addedExpense', (expense, budgetId) => this.updateBudgetExpense(expense, budgetId));
        budgetProvider.getBudgets().subscribe(data => {
            data.forEach(budget => {
                this.categoryProvider.getCategoryForId(budget.categoryId).subscribe(
                    category => {
                        this.budgetData.push({
                            budget: budget,
                            icon: 'md-arrow-down',
                            showDetails: false,
                            categ: category
                        })
                    }
                )
            });
        });
    }

    private toggleDetails(data) {
        if (data.showDetails) {
            data.showDetails = false;
            data.icon = 'md-arrow-down';
        } else {
            data.showDetails = true;
            data.icon = 'md-arrow-up';
        }
    }

    private editBudget(budget: Budget): void {
        this.navCtrl.push(BudgetEditPage, {'budget': budget});
    }

    private addBudget(): void {
        this.navCtrl.push(BudgetAddPage);
    }

    private enableNotifications() {
        this.budgetProvider.enableNotifications(this.allowNotifications).subscribe(res => {
            console.log(res)
        }, err => {
            console.log(err)
        });
    }

    private deleteBudgetPrompt(budget: Budget) {
        let alert = this.alertCtrl.create({
            title: "Delete Budget",
            message: "Really delete budget? This will remove all associated expense logs",
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'Delete',
                    handler: () => this.deleteBudget(budget)
                }
            ]
        });
        alert.present();
    }

    private deleteBudget(budget: Budget) {
        this.budgetProvider.deleteBudget(budget).subscribe(value => {
            let toast = this.toastCtrl.create({
                message: 'Deleted',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
            const index = this.budgetData.findIndex(data => data.budget.id === budget.id);
            this.budgetData.splice(index, 1);
        }, err => {
            let toast = this.toastCtrl.create({
                message: 'Could not delete',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    }

    private addBudgetData(budget: Budget, categ: Category) {
        this.budgetData.push({budget: budget, icon: 'md-arrow-up', showDetails: false, categ: categ})
    }

    private updateBudgetExpense(expense: ExpenseLog, budgetId: number) {
        const data = this.budgetData.find(data => data.budget.id === budgetId);
        data.budget.amountConsumed+=expense.amount;
    }
}

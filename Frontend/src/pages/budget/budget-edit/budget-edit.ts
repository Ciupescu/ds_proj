import {Component} from '@angular/core';
import {Events, NavController, NavParams, ToastController} from 'ionic-angular';
import {Budget} from "../../../domain/budget";
import {BudgetProvider} from "../../../providers/budget/budget";
import {Category} from "../../../domain/category";
import {CategoryProvider} from "../../../providers/category/category";
import {Period} from "../../../domain/period";

@Component({
    selector: 'page-budget-edit',
    templateUrl: 'budget-edit.html',
})
export class BudgetEditPage {

    budget: Budget;
    newCategory: Category;
    newAmount: number;
    newDescription: string;
    newValability: Period;
    categories: Category[] = [];
    valabilities: Period[] = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, private budgetProvider: BudgetProvider, private categoryProvider: CategoryProvider, private toastCtrl: ToastController, private  events:Events) {
        this.budget = navParams.get('budget');
        this.valabilities.push(Period.DAILY);
        this.valabilities.push(Period.WEEKLY);
        this.valabilities.push(Period.MONTHLY);
        this.valabilities.push(Period.YEARLY);
        categoryProvider.getAllSubcategories().subscribe(data => this.categories = data);
    }

    private updateBudget() {
        if (this.newAmount && !Number.isNaN(this.newAmount)) {
            this.budget.amount = this.newAmount;
        }
        if (this.newDescription && this.newDescription.length != 0) {
            this.budget.description = this.newDescription;
        }
        if (this.newCategory) {
            this.budget.categoryId = this.newCategory.id;
        }
        if (this.newValability) {
            this.budget.valability = this.newValability;
        }
        this.budgetProvider.updateBudget(this.budget).subscribe(value => {
            let toast = this.toastCtrl.create({
                message: 'Updated budget',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
            this.events.publish('updatedBudget', this.budget, this.newCategory);
        }, err => {
            let toast = this.toastCtrl.create({
                message: 'Could not update',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    }

    private compareCategories(c1: Category, c2: Category): boolean {
        return c1 && c2 ? c1.id === c2.id : c1 === c2;
    }

    private compareValabilities(v1: Period, v2: Period): boolean {
        return  v1 === v2;
    }
}

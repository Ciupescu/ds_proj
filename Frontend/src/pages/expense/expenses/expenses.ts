import {Component, ViewChild} from '@angular/core';
import {AlertController, Events, NavController, Slides, ToastController} from 'ionic-angular';
import {BudgetProvider} from "../../../providers/budget/budget";
import {Budget} from "../../../domain/budget";
import {ExpenseProvider} from "../../../providers/expense/expense";
import {ExpenseLog} from "../../../domain/expenseLog";
import {RecurringExpense} from "../../../domain/recurringExpense";
import {RecExpenseAddPage} from "../rec-expense-add/rec-expense-add";
import {RecExpenseEditPage} from "../rec-expense-edit/rec-expense-edit";

@Component({
    selector: 'page-expenses',
    templateUrl: 'expenses.html',
})
export class ExpensesPage {
    @ViewChild(Slides) slides: Slides;

    budgets: Budget[] = [];
    selectedBudget: Budget;
    expenseLogs: ExpenseLog[] = [];
    recurringExpenses: RecurringExpense[] = [];
    validRecurringExpenses: RecurringExpense[] = [];
    valid: boolean = false;
    recurring: boolean = false;
    allowNotifications: boolean = true;

    constructor(public navCtrl: NavController, private budgetProvider: BudgetProvider, private expenseProvider: ExpenseProvider, private toastCtrl: ToastController, private alertCtrl: AlertController, private events: Events) {
        events.subscribe('addedRecExpense', recExpense => {
            this.recurringExpenses.push(recExpense);
            this.validRecurringExpenses.push(recExpense)
        });
        events.subscribe('addedBudget', (budget, cat) => {
            this.budgets.push(budget)
        });
        events.subscribe('updatedRecExpense', recExpense => this.updateRecExpensesData(recExpense));
        budgetProvider.getBudgets().subscribe(data => this.budgets = data);
        events.subscribe('updatedBudget', (budget, cat) => this.updateBudgetData(budget));
    }

    private toast(message: string) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }

    private compareBudgets(b1: Budget, b2: Budget): boolean {
        return b1 && b2 ? b1.id === b2.id : b1 === b2;
    }

    private budgetSelected() {
        let budgetId = this.selectedBudget.id;
        this.expenseProvider.getExpensesLogs(budgetId).subscribe(data => this.expenseLogs = data);
        this.expenseProvider.getRecurringExpenses(budgetId).subscribe(data => this.recurringExpenses = data);
        this.expenseProvider.getValidRecurringExpenses(budgetId).subscribe(data => this.validRecurringExpenses = data);
    }

    private setRecurring() {
        this.recurring = this.slides.getActiveIndex() === 1;
    }

    private addExpensePrompt(): void {
        if (!this.selectedBudget) {
            this.toast('Select a budget');
            return;
        }
        if (this.recurring) {
            this.navCtrl.push(RecExpenseAddPage, {'budgetId': this.selectedBudget.id});
        } else {
            let alert = this.alertCtrl.create({
                title: "Add expense log",
                inputs: [
                    {
                        name: 'amount',
                        type: 'number',
                        placeholder: 'amount'
                    },
                    {
                        name: 'description',
                        type: 'text',
                        placeholder: 'description'
                    }
                ],
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                    },
                    {
                        text: 'Add Expense',
                        handler: data => {
                            this.addExpenseLog(data.amount, data.description, this.expenseProvider)
                        }
                    }
                ]
            });
            alert.present();
        }
    }

    private addExpenseLog(amount, description: string, provider: ExpenseProvider) {
        if (!amount || Number.isNaN(amount)) {
            this.toast('Invalid amount');
            return;
        }
        if (!description || description.length === 0) {
            this.toast('Invalid description');
            return;
        }
        const date = new Date();
        const expenseLog: ExpenseLog = {
            id: 0,
            budgetId: this.selectedBudget.id,
            description: description,
            amount: amount,
            timestamp: date.toJSON()
        };
        provider.addExpenseLog(expenseLog).subscribe(value => {
            let toast = this.toastCtrl.create({
                message: 'Added new expense log',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
            expenseLog.id = Number(value);
            expenseLog.timestamp = date.toLocaleString();
            this.expenseLogs.push(expenseLog);
            this.events.publish('addedExpense', expenseLog, expenseLog.budgetId)
        }, err => {
            console.log(err);
            let toast = this.toastCtrl.create({
                message: 'Could not add new expense log',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    }

    private editRecurringExpense(recurringExpense: RecurringExpense): void {
        this.navCtrl.push(RecExpenseEditPage, {"recExpense": recurringExpense});
    }

    private enableNotifications() {
        this.expenseProvider.enableNotifications(this.allowNotifications).subscribe(res => {
            console.log(res)
        }, err => {
            console.log(err)
        });
    }

    private deleteExpenseLogPrompt(expenseLog: ExpenseLog) {
        let alert = this.alertCtrl.create({
            title: "Delete Expense Log",
            message: "Really delete expense log?",
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'Delete',
                    handler: () => this.deleteExpenseLog(expenseLog)
                }
            ]
        });
        alert.present();
    }

    private deleteExpenseLog(expenseLog: ExpenseLog) {
        this.expenseProvider.deleteExpenseLog(expenseLog).subscribe(value => {
            let toast = this.toastCtrl.create({
                message: 'Deleted',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
            const index = this.expenseLogs.findIndex(log => log.id === expenseLog.id);
            this.expenseLogs.splice(index, 1);
        }, err => {
            let toast = this.toastCtrl.create({
                message: "Could not delete expense log",
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    }

    private deleteRecExpensePrompt(recExpense: RecurringExpense) {
        let alert = this.alertCtrl.create({
            title: "Delete Recurring Expense",
            message: "Really delete recurring expense?",
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'Delete',
                    handler: () => this.deleteRecurringExpense(recExpense)
                }
            ]
        });
        alert.present();
    }

    private deleteRecurringExpense(recExpense: RecurringExpense) {
        this.expenseProvider.deleteRecurringExpense(recExpense).subscribe(value => {
            let toast = this.toastCtrl.create({
                message: 'Deleted',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
            const index1 = this.recurringExpenses.findIndex(re => re.id === recExpense.id);
            index1 >=0 ? this.recurringExpenses.splice(index1, 1) : null;
            const index2 = this.validRecurringExpenses.findIndex(re => re.id === recExpense.id);
            index2 >=0 ? this.validRecurringExpenses.splice(index2, 1) : null;
        }, err => {
            let toast = this.toastCtrl.create({
                message: "Could not delete expense log",
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    }

    private updateRecExpensesData(recExpense: RecurringExpense) {
        const index1 = this.recurringExpenses.findIndex(re => re.id === recExpense.id);
        index1 >=0 ? this.recurringExpenses.splice(index1, 1) : null;
        this.recurringExpenses.push(recExpense);
        const index2 = this.validRecurringExpenses.findIndex(re => re.id === recExpense.id);
        index2 >=0 ? this.validRecurringExpenses.splice(index2, 1) : null;
        if (recExpense.expirationDate > new Date()) {
            this.validRecurringExpenses.push(recExpense);
        }
    }

    private updateBudgetData(budget: any) {
        const index1 = this.budgets.findIndex(bu => bu.id === budget.id);
        index1 >=0 ? this.budgets.splice(index1, 1) : null;
        this.budgets.push(budget);
    }
}

import {Component} from '@angular/core';
import {Events, NavController, NavParams, ToastController} from 'ionic-angular';
import {RecurringExpense} from "../../../domain/recurringExpense";
import {ExpenseProvider} from "../../../providers/expense/expense";

@Component({
    selector: 'page-rec-expense-edit',
    templateUrl: 'rec-expense-edit.html',
})
export class RecExpenseEditPage {

    recExpense: RecurringExpense;
    newDescription: string;
    newAmount: number;
    newExpirationDate: Date;

    constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl: ToastController, private expenseProvider: ExpenseProvider, private events: Events) {
        this.recExpense = navParams.get('recExpense');
    }

    private updateRecExpense() {
        if (this.newDescription && this.newDescription.length != 0) {
            this.recExpense.description = this.newDescription;
        }
        if (this.newAmount && !Number.isNaN(this.newAmount)) {
            this.recExpense.amount = this.newAmount;
        }
        if (this.newExpirationDate && this.newExpirationDate > new Date()) {
            this.recExpense.expirationDate = this.newExpirationDate;
        }
        this.expenseProvider.updateRecurringExpense(this.recExpense).subscribe(value => {
            let toast = this.toastCtrl.create({
                message: 'Updated recurring expense',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
            this.events.publish('updatedRecExpense', this.recExpense);
        }, err => {
            let toast = this.toastCtrl.create({
                message: 'Could not update',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    }
}

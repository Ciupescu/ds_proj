import {Component} from '@angular/core';
import {Events, NavController, NavParams, ToastController} from 'ionic-angular';
import {Period} from "../../../domain/period";
import {RecurringExpense} from "../../../domain/recurringExpense";
import {ExpenseProvider} from "../../../providers/expense/expense";

@Component({
    selector: 'page-rec-expense-add',
    templateUrl: 'rec-expense-add.html',
})
export class RecExpenseAddPage {

    newDescription: string;
    newAmount: number;
    newExpirationDate: Date;
    newRecurrence: Period;
    valabilities: Period[] = [];
    budgetId: number;

    constructor(public navCtrl: NavController, public navParams: NavParams, private expenseProvider: ExpenseProvider, private toastCtrl: ToastController, private events: Events) {
        this.budgetId = navParams.get('budgetId');
        this.valabilities.push(Period.DAILY);
        this.valabilities.push(Period.WEEKLY);
        this.valabilities.push(Period.MONTHLY);
        this.valabilities.push(Period.YEARLY);
    }

    private toast(message: string) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }

    private addRecExpense() {
        if (!this.newAmount || Number.isNaN(this.newAmount)) {
            this.toast('Invalid amount');
            return;
        }
        if (!this.newDescription || this.newDescription.length === 0) {
            this.toast('Invalid description');
            return;
        }
        if (!this.newRecurrence) {
            this.toast('Invalid valability');
            return;
        }
        if (!this.newExpirationDate || this.newExpirationDate < new Date()) {
            this.toast('Invalid date');
            return;
        }
        const expense: RecurringExpense = {
            id: 0,
            budgetId: this.budgetId,
            description: this.newDescription,
            amount: this.newAmount,
            lastOccurence: new Date(),
            recurrence: this.newRecurrence,
            expirationDate: this.newExpirationDate
        };
        this.expenseProvider.addRecurringExpense(expense).subscribe(value => {
            let toast = this.toastCtrl.create({
                message: 'Added new recurring expense',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
            expense.id = Number(value);
            this.events.publish('addedRecExpense', expense);
        }, err => {
            let toast = this.toastCtrl.create({
                message: 'Could not add new recurring expense',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    }

    private compareValabilities(v1: Period, v2: Period): boolean {
        return  v1 === v2;
    }

}

import {ErrorHandler, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {MyApp} from './app.component';

import {BudgetsPage} from '../pages/budget/budgets/budgets';
import {CategoriesPage} from '../pages/category/categories/categories';
import {ManagementPage} from '../pages/management/management';
import {TabsPage} from '../pages/tabs/tabs';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {LoginPage} from "../pages/login/login";
import {ExpensesPage} from "../pages/expense/expenses/expenses";

import {BudgetProvider} from '../providers/budget/budget';
import {HttpClientModule} from "@angular/common/http";

import {HTTP} from "@ionic-native/http";
import {BudgetEditPage} from "../pages/budget/budget-edit/budget-edit";
import {BudgetAddPage} from "../pages/budget/budget-add/budget-add";
import {CategoryProvider} from '../providers/category/category';
import {ExpenseProvider} from '../providers/expense/expense';
import {CategoryAddPage} from "../pages/category/category-add/category-add";
import {CategoryEditPage} from "../pages/category/category-edit/category-edit";
import {RecExpenseEditPage} from "../pages/expense/rec-expense-edit/rec-expense-edit";
import {RecExpenseAddPage} from "../pages/expense/rec-expense-add/rec-expense-add";

HTTP.getPluginRef = () => "cordova.plugin.http";

@NgModule({
    declarations: [
        MyApp,
        BudgetsPage,
        CategoriesPage,
        ExpensesPage,
        ManagementPage,
        TabsPage,
        LoginPage,
        BudgetEditPage,
        BudgetAddPage,
        CategoryAddPage,
        CategoryEditPage,
        RecExpenseAddPage,
        RecExpenseEditPage
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        HttpClientModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        BudgetsPage,
        CategoriesPage,
        ExpensesPage,
        ManagementPage,
        LoginPage,
        TabsPage,
        BudgetEditPage,
        BudgetAddPage,
        CategoryAddPage,
        CategoryEditPage,
        RecExpenseAddPage,
        RecExpenseEditPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        BudgetProvider,
        HTTP,
        CategoryProvider,
        ExpenseProvider
    ]
})
export class AppModule {
}
